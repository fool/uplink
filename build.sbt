
name := "uplink"

version := "1.9"

scalaVersion := "2.11.7"


libraryDependencies ++= Seq(
  "org.clapper"               % "grizzled-slf4j_2.11"  % "1.0.2",
  "org.apache.httpcomponents" %  "httpclient"          % "4.5",
  "org.apache.httpcomponents" %  "httpmime"            % "4.5",
  "org.scalaz"                % "scalaz-core_2.11"     % "7.1.3",
  "ch.qos.logback"            %  "logback-classic"     % "1.1.3",
  "commons-cli"               %  "commons-cli"         % "1.3.1",
  "org.json4s"                % "json4s-native_2.11"   % "3.3.0"
)


scalacOptions ++= Seq("-unchecked", "-deprecation")
