Uplink
===
by jordan sterling
8/15/15


Uploads files to a box folder. Recursively checks contents are there and validates sha1s.


Example:
```
java -jar ./target/scala-2.11/uplink-assembly-1.0.jar \
  --client-id "v498zpnc1qnfm3r2qvvrns3umv642fd1" \
  --client-secret "rUVTosBlV3EiokarKRcjVOHvAAqUKqvX" \
  --username "myboxaccount@gmail.com" \
  --password "hunter2" \
  --folder "/Where/I/Keep/My/Stuff" \
  --box-folder "4231296775"
```

This will upload all files and subfolders of 'Stuff' to the given box folder id.

Use a box folder id of '0' to compare to the root folder.

- Any folder that already exists will also have its files checked.
- Any folder that doesn't exist on box will be created, and all sub files uploaded.
- Any files that already exist will have their SHA1 compared to make sure the same file is on both sides.

This is safe and best to run a couple times to ensure that everything really was uploaded. The subsequent runs
are fast since there's less to upload, the time comes from  calculating local sha1.


Building
===
`sbt compile`
to make jar
`sbt assembly`
