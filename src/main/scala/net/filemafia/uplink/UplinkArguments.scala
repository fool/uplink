package net.filemafia.uplink

import java.io.File

import grizzled.slf4j.Logging
import net.filemafia.fs.{MegaBytes, FileSize}
import net.filemafia.http.UplinkException
import org.apache.commons.cli.{Option => CliOption, HelpFormatter, UnrecognizedOptionException, CommandLine, OptionBuilder, DefaultParser, Options}
import UplinkArguments.{DefaultLogLevel, DefaultMulti, DefaultRedirectUri, DefaultState, DefaultHttpRetries}
import scala.util.{Success, Failure, Try}
import net.filemafia.uplink.MultithreadedRunner.DefaultBigFileCutoff
import net.filemafia.http.ProgressStep.DefaultProgressPercent

case class UplinkArguments(
  errors:       Seq[String] = Seq.empty,
  clientId:     String = "",
  clientSecret: String = "",
  username:     String = "",
  password:     String = "",
  localFolder:  File = new File("."),
  boxFolderId:  String = "",
  multi:        Int =    DefaultMulti,
  logLevel:     String = DefaultLogLevel,
  help:         Boolean = false,
  state:        String = DefaultState,
  redirectUri:  String = DefaultRedirectUri,
  sizeLimit:    FileSize =   DefaultBigFileCutoff,  // megabytes
  progressPercent: Int = DefaultProgressPercent,
  httpRetries:  Int = DefaultHttpRetries
)


object UplinkArguments
extends Logging {

  val DefaultMulti = 5
  val DefaultLogLevel = "INFO"
  val DefaultState = "state"
  val DefaultRedirectUri = "http://localhost"
  val DefaultHttpRetries = 3

  object Options {
    val ClientId     = "client-id"
    val ClientSecret = "client-secret"
    val Username     = "username"
    val Password     = "password"
    val Folder       = "folder"
    val BoxFolder    = "box-folder"
    val Multi        = "multi"
    val LogLevel     = "log-level"
    val Help         = "help"
    val State        = "state"
    val RedirectUri  = "redirect-uri"
    val SizeLimit    = "progress-size"
    val ProgressPercent  = "progress-percent"

    val Required = Seq(ClientId, ClientSecret, Username, Password, Folder, BoxFolder)
  }
  val LogLevels = Seq("ERROR", "WARN", "INFO", "DEBUG", "TRACE")

  def help(): UplinkArguments = UplinkArguments(help=true)


  def parse(args: Seq[String]): UplinkArguments = {
    val options = new Options()

    val clientId     = CliOption.builder("c").longOpt(Options.ClientId).hasArg.argName("CLIENT ID").desc("*Required. Box client id").build()
    val clientSecret = CliOption.builder("s").longOpt(Options.ClientSecret).hasArg.argName("CLIENT SECRET").desc("*Required. Box client secret").build()
    val username     = CliOption.builder("u").longOpt(Options.Username).hasArg.argName("USERNAME").desc("*Required. Box username (email)").build()
    val password     = CliOption.builder("p").longOpt(Options.Password).hasArg.argName("PASSWORD").desc("*Required. Box password").build()
    val localFolder  = CliOption.builder("f").longOpt(Options.Folder).hasArg.argName("PATH").desc("*Required. Local folder to sync").build()
    val boxFolder    = CliOption.builder("b").longOpt(Options.BoxFolder).hasArg.argName("ID").desc("*Required. Box folder id").build()
    val multi        = CliOption.builder().longOpt(Options.Multi).hasArg.argName("NUM").desc(s"How many uploads to run in parallel. Default: $DefaultMulti")
                                .required(false).build()
    val logLevel     = CliOption.builder().longOpt(Options.LogLevel).hasArg.argName("LEVEL").desc(s"Log level: ${LogLevels.mkString(", ")}. Defualt: $DefaultLogLevel")
                                .required(false).build()
    val help         = CliOption.builder("h").longOpt(Options.Help).desc(s"Show this help").required(false).build()
    val redirectUri  = CliOption.builder().longOpt(Options.RedirectUri).hasArg.argName("URI").desc(s"Box OAuth API redirect_uri value")
                                .required(false).build()
    val state        = CliOption.builder().longOpt(Options.State).hasArg.argName("STATE").desc(s"Box OAuth API State value")
                                .required(false).build()
    val sizeLimit    = CliOption.builder().longOpt(Options.SizeLimit).hasArg.argName("MEGABYTES").desc(s"Only display progress for files bigger than MEGABYTES. Default: 500MB")
                                .required(false).build()
    val progressPercent = CliOption.builder().longOpt(Options.ProgressPercent).hasArg.argName("INT").desc(s"Receive progress updates every __%")
                                .required(false).build()

    options.addOption(clientId)
    options.addOption(clientSecret)
    options.addOption(username)
    options.addOption(password)
    options.addOption(localFolder)
    options.addOption(boxFolder)
    options.addOption(multi)
    options.addOption(logLevel)
    options.addOption(help)
    options.addOption(redirectUri)
    options.addOption(state)
    options.addOption(sizeLimit)
    options.addOption(progressPercent)

    val parser = new DefaultParser()
    Try(parser.parse(options, args.toArray)) match {
      case Success(cmd) =>
        val missingOptions = Options.Required.filterNot(cmd.hasOption)
        if (missingOptions.nonEmpty) {
          showHelp(options)
          UplinkArguments(errors=missingOptions.map(e => s"Missing required option: $e"))
        } else {
          val help = cmd.hasOption(Options.Help)
          val logLevel = cmd.getOptionValue(Options.LogLevel, DefaultLogLevel).trim.toUpperCase
          val sizeLimit = Try(FileSize(cmd.getOptionValue(Options.SizeLimit, DefaultBigFileCutoff.megaBytes.toInt.toString).toLong, MegaBytes)).toOption
          val progressPercent = Try(cmd.getOptionValue(Options.ProgressPercent, DefaultProgressPercent.toString).toInt).toOption
          if (!LogLevels.contains(logLevel)) {
            UplinkArguments(errors = Seq(s"Invalid log level: $logLevel. Must be one of ${LogLevels.mkString(", ")}"))
          } else if (!sizeLimit.exists(_.bits >= 0)) {
            UplinkArguments(errors = Seq(s"Invalid size limit: $sizeLimit. Must be greater than or equal to zero."))
          } else if (!progressPercent.exists(p => p >= 0 && p <= 100)) {
            UplinkArguments(errors = Seq(s"Invalid progress percent $progressPercent, must be 0 <= progress <= 100"))
          } else if (help) {
            showHelp(options)
            UplinkArguments.help()
          } else {
            val mul = cmd.getOptionValue(Options.Multi, DefaultMulti.toString)
            val state = cmd.getOptionValue(Options.State, DefaultState)
            val redirectUri = cmd.getOptionValue(Options.RedirectUri, DefaultRedirectUri)
            UplinkArguments(
              missingOptions,
              getOrElseThrow(cmd, Options.ClientId),
              getOrElseThrow(cmd, Options.ClientSecret),
              getOrElseThrow(cmd, Options.Username),
              getOrElseThrow(cmd, Options.Password),
              new File(getOrElseThrow(cmd, Options.Folder)),
              getOrElseThrow(cmd, Options.BoxFolder),
              mul.toInt,
              logLevel,
              help,
              state,
              redirectUri,
              sizeLimit.get,
              progressPercent.get
            )
          }
        }
      case Failure(e: UnrecognizedOptionException) =>
        UplinkArguments(errors=Seq(e.getMessage))
      case Failure(t) => throw t
    }
  }

  def showHelp(options: Options): Unit = {
    val formatter = new HelpFormatter()
    formatter.printHelp("java -jar uplink.jar [OPTIONS] ", options )
  }

  def getOrElseThrow(cmd: CommandLine, name: String): String = {
    Option(cmd.getOptionValue(name)).getOrElse(throw MissingArgument(name))
  }
}

case class MissingArgument(name: String) extends UplinkException(s"Missing required argument: $name")
