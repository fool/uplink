package net.filemafia.uplink

import grizzled.slf4j.Logging
import net.filemafia.box.BoxApi
import net.filemafia.uplink.JobResults.{Interrupt, Success, Failure}

case class Worker[J <: Job[_, _]](job: J) {
  val thread = new Thread(job)
  def start(): Unit = thread.start()
  def stop(): Unit = {
    job.stop()
  }
  def alive: Boolean = thread.isAlive
  def status = job.status
}


class JobManager[R <: Request, J <: Job[R, JC], JC <: JobCreator[R]](jobCreator: JC,
                                                                       boxApi: BoxApi,
                                                                       maxWorkers: Int,
                                                                       workerCreator: (JC, BoxApi) => J)
  extends Logging {
  protected var running = true
  protected var _interrupt = false

  def interrupt(): Unit = if (!_interrupt) _interrupt = true

  protected var workers: Seq[Worker[J]] = Seq.empty
  def run(): JobResults = {
    topOffWorkers()
    while (keepGoing) {
      logger.debug(s"Waiting on ${workers.count(_.alive)} threads: ${workers.map(_.status)}")
      topOffWorkers()
      Thread.sleep(1000)
      if (workers.forall(_.status == WaitingForJob)) {
        logger.debug("=====> Detected that all workers are WaitingForJob, stopping them")
        workers.foreach(_.stop())
      }
      if (_interrupt) {
        logger.debug("=====> Detected interrupt, stopping all workers")
        workers.foreach(_.stop())
      }
      workers = workers.filter(_.alive)
    }
    if (_interrupt)                     Interrupt
    else if (Metrics.totalErrors == 0L) Success
    else                                Failure
  }

  def keepGoing: Boolean = {
    if (_interrupt) {
      logger.info("Shutting down due to interrupt")
      false
    } else {
      workers.exists(_.alive)
    }
  }

  def topOffWorkers(): Unit = {
    val missingWorkers = maxWorkers - workers.size
    if (missingWorkers > 0) {
      val newWorkers =
        (0 to missingWorkers - 1).map { _ =>
          val job = workerCreator(jobCreator, boxApi)
          Worker(job)
                                      }

      logger.debug(s"Creating ${newWorkers.size} new workers")
      newWorkers.foreach(_.start())
      workers ++= newWorkers
    }
  }
}



sealed trait JobResults
object JobResults {
  case object Success extends JobResults
  case object Failure extends JobResults
  case object Interrupt extends JobResults
}

// do not make case class, this holds state
class FileSystemJobManager(jobCreator: FileSystemJobCreator,
                           boxApi: BoxApi,
                           maxWorkers: Int)
extends JobManager[FileSystemRequest, SyncerJob, FileSystemJobCreator](jobCreator, boxApi, maxWorkers, SyncerJob.apply)
