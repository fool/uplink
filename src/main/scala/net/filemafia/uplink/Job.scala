package net.filemafia.uplink

import java.io.File

trait Job[R <: Request, C <: JobCreator[R]] extends Runnable {
  def jobCreator: C

  def run(): Unit

  def status: JobStatus

  def stop(): Unit

  protected def execute(request: R): Response
}

sealed trait JobStatus
case object Running        extends JobStatus
case object GettingNextJob extends JobStatus
case object WaitingForJob  extends JobStatus
case object Finished       extends JobStatus


trait JobCreator[R <: Request] {
  def nextJob(): Option[R]
}

trait Request

trait Response {
  def success: Boolean
  def message: String
  def newJobs: Seq[Request]
}
