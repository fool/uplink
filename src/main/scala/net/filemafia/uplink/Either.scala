package net.filemafia.uplink

import net.filemafia.uplink.Either.{UplinkEither, FutureEither}
import scala.util.Properties
import scala.concurrent.Future
import scalaz._, Scalaz._
import scala.concurrent.ExecutionContext.Implicits.global

object Either {

  type UplinkEither[R] = UplinkError \/ R

  type FutureEither[R] = EitherT[Future, UplinkError, R]

  type ReaderEither[D, R] = Reader[D, FutureEither[R]]
}

object FutureEither {
  def constraint[R](inner: R, predicate: R => Boolean, error: UplinkError): FutureEither[R] = {
    for {
      uplinkEither <-
        if (predicate(inner)) Conversions.futureEitherSuccess(inner)
        else Conversions.futureEitherFailure(error)
    } yield uplinkEither
  }
}

trait UplinkError {
  def message: String
}

case class FutureFailedWithException(error: UplinkError, throwable: Throwable) extends UplinkError {
  val message = s"$error\n${throwable.getMessage}\n${throwable.getStackTrace.mkString("", Properties.lineSeparator, Properties.lineSeparator)}"
  override def toString = message
}

object Conversions {
  def futureEitherSuccess[R](right: R): FutureEither[R] = {
    EitherT.eitherT(Future[UplinkEither[R]](\/.right(right)))
  }

  def futureEitherFailure[Right](left: UplinkError): FutureEither[Right] = {
    EitherT.eitherT(Future[UplinkEither[Right]](\/.left(left)))
  }

  def toFutureEither[Right](either: UplinkEither[Right], error: UplinkError): FutureEither[Right] = {
    futureToFutureEither(Future(either), error)
  }

  def blockToFutureEither[Right](block: => () => Right, error: UplinkError): FutureEither[Right] = {
    futureToFutureEither(Future[UplinkEither[Right]](success(block())), error)
  }

  def futureToFutureEither[Right](future: Future[UplinkEither[Right]], error: UplinkError): FutureEither[Right] = {
    val r: Future[UplinkEither[Right]] = future.recover {
      case t: Throwable => \/.left(FutureFailedWithException(error, t))
    }
    EitherT.eitherT(r)
  }

  def success[Right](right: Right): UplinkEither[Right] = {
    \/.right(right)
  }

  def failure[Right](left: UplinkError): UplinkEither[Right] = {
    \/.left(left)
  }

}

object ImplicitConversions {
  implicit class FutureUplinkEitherToFutureEither[Right](uplinkEither: Future[UplinkEither[Right]]) {
    def asFutureEither(error: UplinkError): FutureEither[Right] = {
      Conversions.futureToFutureEither(uplinkEither, error)
    }
  }

  implicit class UplinkEitherMaker[A](value: A) {
    def uplinkRight(): UplinkEither[A] = Conversions.success(value)
  }

  def uplinkLeft[R <: UplinkError](error: UplinkError): UplinkEither[R] = Conversions.failure(error)

  implicit class EitherOption[A](value: Option[A]) {
    def getOrUplinkError(error: UplinkError): UplinkEither[A] = {
      value.map(Conversions.success).getOrElse(Conversions.failure(error))
    }
  }
}