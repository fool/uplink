package net.filemafia.uplink

import sun.misc.{Signal, SignalHandler}
import grizzled.slf4j.Logging

class UplinkSignalHandler(jobManager: JobManager[_, _, _]) extends SignalHandler with Logging {
  def handle(signal: Signal): Unit = {
    logger.info(s"Signal received: ${signal.getNumber} ${signal.getName}")
    jobManager.interrupt()
  }
}


