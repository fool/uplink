package net.filemafia.uplink

import java.io.File

import grizzled.slf4j.Logging
import net.filemafia.box.{LocalFolderIsRemoteFileError, FolderType, FileType, LocalFileIsRemoteFolderError, BoxApi, UploadResponse, ContentApi}
import net.filemafia.fs.Checksum
import net.filemafia.uplink.Either.{FutureEither, ReaderEither}
import scalaz._, Scalaz._
import scala.concurrent.ExecutionContext.Implicits.global

object Synchronizer
extends Logging {

  /** this only happens to the very first item. the first one we have a local file but only a string for box id */
  def addItemForSync(request: LocalFileSystemRequest): ReaderEither[BoxApi, SyncerResponse] = {
    Reader((clients) => {
      for {
        folder <- ContentApi.getBoxFolder(request.boxFolderId)(clients)
        newJobs = FileSystemJobCreator.localDirectoryJobs(request.localFile, folder)
        _ = {
          val missingJobs = folder.items.filterNot(boxItem => newJobs.exists(_.localFile.getName == boxItem.name))
          missingJobs.foreach(item => logger.warn(s"Item on box is missing from local machine: $item"))
        }
      } yield SyncerResponse(request.localFile, true, s"Looked up remote folder ${request.boxFolderId}", newJobs)
    })
  }

  def syncItem(request: RemoteFileSystemRequest): ReaderEither[BoxApi, SyncerResponse] = {
    Reader((clients) => {
      logger.debug(s"Starting syncFile for ${request.localFile} and ${request.boxFolder.id}")
      if (request.localFile.isDirectory) {
        syncFolder(request)(clients)
      } else {
        syncFile(request)(clients)
      }
    })
  }

  def syncFolder(request: RemoteFileSystemRequest): ReaderEither[BoxApi, SyncerResponse] = {
    Reader((boxApi) => {
      Metrics.incrementTotalFolders()
      val folderName = request.localFile.getName
      for {
        remoteFolder <- request.boxFolder.subItem(folderName) match {
          case Some (item) if item.itemType == FolderType =>
            Metrics.incrementTotalExistingFolders ()
            ContentApi.getBoxFolder(item.id)(boxApi)

          case Some (item) if item.itemType == FileType =>
            Conversions.futureEitherFailure(LocalFolderIsRemoteFileError(folderName, request.boxFolder, item))

          case None =>
            Metrics.incrementTotalFoldersCreated ()
            logger.info (s"Creating a new box folder inside ${request.boxFolder.id}: $folderName")
            ContentApi.createBoxFolder(folderName, request.boxFolder)(boxApi)
        }
        subFiles = {
          logger.info(s"Adding ${request.localFile.listFiles().length} subfiles to check from $folderName")
          FileSystemJobCreator.localDirectoryJobs(request.localFile, remoteFolder)
        }
      } yield SyncerResponse(request.localFile, true, s"Folder recursion box id: ${remoteFolder.id} add ${subFiles.length} sub files", subFiles)
    })
  }

  def syncFile(r: RemoteFileSystemRequest): ReaderEither[BoxApi, SyncerResponse] = {
    Reader((clients) => {
      Metrics.incrementTotalFiles()
      val localFileName = r.localFile.getName
      for {
        fileResult <- {
          r.boxFolder.subItem(localFileName) match {
            case Some(existingFolder) if existingFolder.itemType == FolderType =>
              logger.error(s"A local file is a folder on box. In box folder ${r.boxFolder.id} Folder ${existingFolder.id} exists with name: $localFileName but is local file: ${r.localFile.getAbsolutePath}")
              Conversions.futureEitherFailure(LocalFileIsRemoteFolderError(r.boxFolder, existingFolder, r.localFile))

            case Some(existingFile) if existingFile.itemType == FileType =>
              logger.debug(s"File already exists with name $localFileName in folder ${r.boxFolder.id}, file id: ${existingFile.id} , checking if hash matches")
              for {
                existingFileInfo <- ContentApi.fileInfo(existingFile.id)(clients)
                response <- checkIfRemoteFileMatchesLocal(r.localFile, existingFileInfo.sha1)
              } yield response

            case None =>
              logger.info(s"Uploading file: ${r.localFile.getPath} to ${r.boxFolder.id}")
              for {
                uploadResponse <- ContentApi.upload(r.localFile, r.boxFolder)(clients)
                success <- parseUploadResponse(uploadResponse, r.localFile)
              } yield success
          }
        }
      } yield fileResult
    })
  }

  /** this can probably move to ContentApi */
  def parseUploadResponse(uploadResponse: UploadResponse, file: File): FutureEither[SyncerResponse] = {
    logger.trace(s"$file upload complete: uploadResponse.content")
    if (uploadResponse.httpStatus == 201) {
      Metrics.incrementTotalUploads()
      logger.debug(s"Uploaded sucessfully $file")
      Conversions.futureEitherSuccess(SyncerResponse(file, true, s"Uploaded successfully"))
    } else if (uploadResponse.httpStatus == 409) {
      logger.debug("Got 409, checking to see if file is already there")
      for {
        remoteFileMatchesLocal <- checkIfRemoteFileMatchesLocal(file, uploadResponse.sha1)
      } yield remoteFileMatchesLocal
    } else {
      logger.error(s"Upload failed because $uploadResponse")
      Conversions.futureEitherSuccess(SyncerResponse(file, false, s"Upload failed because $uploadResponse"))
    }
  }

  def checkIfRemoteFileMatchesLocal(file: File, remoteSha1: String): FutureEither[SyncerResponse] = {
    for {
      localSha1 <- Checksum.sha1(file)
      good = localSha1.equalsIgnoreCase(remoteSha1)
      res = {
        if (good) {
          Metrics.incrementTotalExisting()
          logger.debug(s"File already uploaded $file")
          SyncerResponse(file, true, s"File already uploaded, remote sha1 matches local")
        } else {
          logger.error(s"SHA MISMATCH - $file   \nRemote sha1: $remoteSha1\nLocal sha1: $localSha1")
          SyncerResponse(file, false, s"SHA MISMATCH remoteSha1: $remoteSha1  local: $localSha1")
        }
      }
    } yield res
  }
}


case class ShaCheckError(file: File, sha1: String) extends UplinkError {
  val message = s"Unable to calculate sha1 on local file: $file with remote sha1: $sha1"
}