package net.filemafia.uplink

import grizzled.slf4j.Logging
import net.filemafia.box.{BoxApi, AccessToken}
import net.filemafia.fs._
import org.apache.http.client.HttpClient
import sun.misc.Signal
import scala.concurrent.duration._

object MultithreadedRunner
extends Logging {

  val DefaultBigFileCutoff  = FileSize(500, MegaBytes)

  def begin(uplinkArguments: UplinkArguments, httpClient: HttpClient, accessToken: AccessToken): Int = {
    val jobCreator = new FileSystemJobCreator
    val boxApi     = BoxApi(httpClient, accessToken, uplinkArguments)
    val jobManager = new FileSystemJobManager(jobCreator, boxApi, uplinkArguments.multi)
    jobCreator.addJob(LocalFileSystemRequest(uplinkArguments.localFolder, uplinkArguments.boxFolderId))

    Signal.handle(new Signal("INT"), new UplinkSignalHandler(jobManager))

    val start = System.currentTimeMillis()
    val results = jobManager.run()
    val duration = new FiniteDuration(System.currentTimeMillis()  - start, MILLISECONDS)

    printStats(results, duration)
    if (Metrics.totalErrors > 0) {
      8
    } else {
      0
    }
  }

  def printStats(results: JobResults, duration: Duration): Unit = {
    logger.info("")
    logger.info(s"Finished: ${results.toString.toUpperCase}")
    logger.info(lowestUnitPrint(duration))
    logger.info("")
    logger.info(s"Total items in local folder:  ${Metrics.totalFiles + Metrics.totalFolders}")
    logger.info(s"  Local Files:      ${Metrics.totalFiles}")
    logger.info(s"  Local Folders:    ${Metrics.totalFolders}")
    logger.info("")
    logger.info(s"Already existed remotely:     ${Metrics.totalExisting + Metrics.totalExistingFolders}")
    logger.info(s"  Existing files:   ${Metrics.totalExisting}")
    logger.info(s"  Existing folders: ${Metrics.totalExistingFolders}")
    logger.info(s"")
    logger.info(s"Created items:                ${Metrics.totalUploads + Metrics.totalFoldersCreated}")
    logger.info(s"  Uploaded files:   ${Metrics.totalUploads}")
    logger.info(s"  Created folders:  ${Metrics.totalFoldersCreated}")
    logger.info("")
    logger.info(s"Total items in remote folder: ${Metrics.totalExisting + Metrics.totalExistingFolders + Metrics.totalUploads + Metrics.totalFoldersCreated}")
    logger.info(s"  Remote files:     ${Metrics.totalUploads + Metrics.totalExisting}")
    logger.info(s"  Remote folders:   ${Metrics.totalFoldersCreated + Metrics.totalExistingFolders}")
    logger.info(s"")
    logger.info(s"Errors:                       ${Metrics.totalErrors}")
    logger.info(s"")
    logger.info(s"Stats:")
    logger.info(f"  Sha1 Rate:   ${Metrics.sha1RatePerSecond.lowestUnitString(3)}/s " +
                f"Digested ${Metrics.sha1Bytes.lowestUnitString(3)} in ${lowestUnitPrint(Metrics.durationCalculatingSha1)}")
    logger.info(f"  Upload Rate: ${Metrics.uploadRatePerSecond.lowestUnitString(3)}/s " +
                f"Uploaded ${Metrics.uploadedBytes.lowestUnitString(3)} in ${lowestUnitPrint(Metrics.durationUploading)}")
    logger.info(s"")
  }

  def lowestUnitPrint(duration: Duration): String = {
    val u = lowestUnit(duration)
    f"${duration.toUnit(u)}%.2f ${u.toString.toLowerCase}"
  }

  def lowestUnit(duration: Duration): TimeUnit = {
    if        (duration.toDays > 5) {
      DAYS
    } else if (duration.toHours > 24) {
      HOURS
    } else if (duration.toMinutes > 5) {
      MINUTES
    } else if (duration.toSeconds > 1) {
      SECONDS
    } else if (duration.toMillis > 10) {
      MILLISECONDS
    } else if (duration.toMicros > 10) {
      MICROSECONDS
    } else if (duration.toNanos > 0) {
      NANOSECONDS
    } else {
      SECONDS
    }
  }
}
