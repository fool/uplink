package net.filemafia.uplink

import grizzled.slf4j.Logging
import net.filemafia.uplink.UplinkApplication.ExitCode
import org.slf4j.LoggerFactory
import ch.qos.logback.classic.{Logger=>LogbackLogger,Level}
import org.slf4j.Logger
import net.filemafia.box.{Box, AccessToken, LoginParams}
import scala.concurrent.duration.{FiniteDuration, SECONDS, DAYS}
import net.filemafia.http.HttpClientMaker
import scala.util.Properties

object UplinkApplication {

  object ExitCode {
    val Success = 0
    val ArgumentError = 1
    val TopLevelException = 3
  }

  val logLevelProperty = "org.slf4j.simpleLogger.defaultLogLevel"
  def main(args: Array[String]): Unit = {
    val exitCode: Int = try {
      val arguments = UplinkArguments.parse(args)
      if (arguments.help) {
        ExitCode.Success
      } else {
        // I don't know how to use logback properly...
        LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)
                     .asInstanceOf[LogbackLogger]
                     .setLevel(Level.valueOf(arguments.logLevel))
        Uplink.run(arguments)
      }
    } catch {
      case t: Throwable =>
        println(s"error=> at top level ${t.getMessage} ${t.getStackTrace.mkString("", Properties.lineSeparator, Properties.lineSeparator)}")
        ExitCode.TopLevelException
    }
    System.exit(exitCode)
  }
}


object Uplink
extends Logging {
  def run(arguments: UplinkArguments): Int = {
    if (arguments.errors.nonEmpty) {
      arguments.errors.foreach(msg => logger.warn(msg))
      ExitCode.ArgumentError
    } else {
      val params = LoginParams(arguments.clientId, arguments.clientSecret, arguments.username, arguments.password, arguments.state, arguments.redirectUri)
      val client = HttpClientMaker.httpClient(arguments)
      val accessToken = AccessToken(params)
      MultithreadedRunner.begin(arguments, client, accessToken)
    }
  }

  /**
   * since this is likely the source of the duration single-thread lock it gets its own timeout.
   * every hour the access token expires and when it does one thread will get synchronized status to fetch a new one
   */
  val AccessTokenTimeout = new FiniteDuration(60, SECONDS)

  /**
   * How long one file can take to upload and verify its sha1
   */
  val SingleFileTimeout = Box.RefreshTokenLifetime
}
