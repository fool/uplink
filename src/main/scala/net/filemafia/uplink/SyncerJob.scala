package net.filemafia.uplink

import java.io.File
import grizzled.slf4j.Logging
import net.filemafia.box.{BoxFolder, BoxApi}
import net.filemafia.http.UplinkException
import scala.concurrent.Await
import scalaz._, Scalaz._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.{Queue=>MutableQueue}

case class SyncerResponse(file: File, success: Boolean, message: String, newJobs: Seq[FileSystemRequest] = Seq.empty) extends Response

object SyncerResponse {
  def errorResponse(error: String) = SyncerResponse(new File("."), false, error)
}

/** workers.. these are 1 to 1 with threads */
case class SyncerJob(fsjc: FileSystemJobCreator, boxApi: BoxApi)
extends    Job[FileSystemRequest, FileSystemJobCreator]
with       Logging {
  import SyncerJob._

  val jobCreator: FileSystemJobCreator = fsjc

  private var myStatus: JobStatus = Running

  def run(): Unit = {
    try {
      listenForNewJobsForever()
      myStatus = Finished
      logger.debug("SyncerJob exiting successfully")
    } catch {
      case t: Throwable => logger.error("Exception blow up a SyncerResponse", t)
    }
  }

  def stop(): Unit =
    keepGoing = false

  /** this is a flag that lets someone shut down a worker */
  private var keepGoing = true

  def status = myStatus

  protected def listenForNewJobsForever(): Unit = {
    while (keepGoing) {
        jobCreator.nextJob() match {
          case Some(request) =>
            myStatus = Running
            val response = execute(request)
            val message = s"File complete! Success? ${response.success} ${response.message} ${response.file} "
            if (response.success) {
              logger.info(message)
            } else {
              Metrics.incrementTotalErrors()
              logger.error(message)
            }
            myStatus = GettingNextJob
          case None =>
            myStatus = WaitingForJob
            Thread.sleep(WaitTimeMs)
        }
    }
  }

  protected def execute(request: FileSystemRequest): SyncerResponse = {
    val futureResponse = request match {
      case local: LocalFileSystemRequest => Synchronizer.addItemForSync(local)(boxApi)
      case remote: RemoteFileSystemRequest => Synchronizer.syncItem(remote)(boxApi)
    }

    Await.result(futureResponse.run, Uplink.SingleFileTimeout) match {
      case \/-(response) =>
        jobCreator.addJobs(response.newJobs)
        response
      case -\/(error) =>
        logger.error(s"Job failed: $request, $error")
        SyncerResponse(request.localFile, false, s"Job failed: $request, $error")
    }
  }
}


class FileSystemJobCreator extends JobCreator[FileSystemRequest] {

  private val requests = new MutableQueue[FileSystemRequest]()


  def addJobs(jobs: Seq[FileSystemRequest]): Unit = {
    jobs.foreach(requests.enqueue(_))
  }

  def addJob(job: FileSystemRequest): Unit =
    synchronized { requests.enqueue(job) }

  def nextJob(): Option[FileSystemRequest] = {
    synchronized {
      try {
        Some(requests.dequeue())
      } catch {
        case e: NoSuchElementException => None
      }
    }
  }
}

object FileSystemJobCreator {
  def localDirectoryJobs(localFolder: File, remoteFolder: BoxFolder): Seq[RemoteFileSystemRequest] = {
    if (!localFolder.isDirectory || !localFolder.exists()) {
      throw InvalidDirectoryException(localFolder)
    }
    Option(localFolder.listFiles()).map {
      files: Array[File] =>
        files.toSeq.map(RemoteFileSystemRequest(_, remoteFolder))
    }.getOrElse(Seq.empty)
  }
}

case class InvalidDirectoryException(file: File)
extends UplinkException(s"Not  directory: ${file.getAbsolutePath}")

object SyncerJob {
  val WaitTimeMs = 500
}

sealed trait FileSystemRequest extends Request {
  def localFile: File
}

case class LocalFileSystemRequest(localFile: File, boxFolderId: String)
extends    FileSystemRequest

case class RemoteFileSystemRequest(localFile: File, boxFolder: BoxFolder)
extends    FileSystemRequest

