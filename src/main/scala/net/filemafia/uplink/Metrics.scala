package net.filemafia.uplink
import net.filemafia.fs.{Bytes, FileRate, FileSize}

import scala.concurrent.duration.{FiniteDuration, MILLISECONDS, Duration}
import grizzled.slf4j.Logging

/**
 * Ghetto metrics
 */
object Metrics extends Logging {
  
  protected var _totalFiles: Long               = 0
  protected var _totalFolders: Long             = 0
  protected var _totalFoldersCreated: Long      = 0
  protected var _totalUploads: Long             = 0
  protected var _totalExisting: Long            = 0
  protected var _totalExistingFolders: Long     = 0
  protected var _totalErrors: Long              = 0
  protected var _durationUploading: Long        = 0
  protected var _totalBytesUploaded: Long       = 0
  protected var _durationCalculatingSha1: Long  = 0
  protected var _totalBytesCalculatedSha1: Long = 0


  def incrementTotalFiles(): Unit = {
    synchronized( { _totalFiles += 1 })
  }

  def incrementTotalFolders(): Unit = {
    synchronized( { _totalFolders += 1 })
  }

  def incrementTotalFoldersCreated(): Unit = {
    synchronized( { _totalFoldersCreated += 1 })
  }

  def incrementTotalUploads(): Unit = {
    synchronized( { _totalUploads += 1 })
  }

  def incrementTotalExisting(): Unit = {
    synchronized( { _totalExisting += 1 })
  }

  def incrementTotalExistingFolders(): Unit = {
    synchronized( { _totalExistingFolders += 1 })
  }

  def incrementTotalErrors(): Unit = {
    synchronized( { _totalErrors += 1 })
  }

  def incrementSha1Bytes(amount: Long = 1): Unit = {
    synchronized( { _totalBytesCalculatedSha1 += amount })
  }
  def incrementUploadedBytes(amount: Long = 1): Unit = {
    synchronized( { _totalBytesUploaded += amount })
  }

  def timeUpload[A](callback: () => A): A = {
    val now = System.currentTimeMillis()
    val r = callback()
    synchronized({ _durationUploading += System.currentTimeMillis() - now})
    r
  }

  def timeSha1[A](callback: () => A): A = {
    val now = System.currentTimeMillis()
    val r = callback()
    synchronized({ _durationCalculatingSha1 += System.currentTimeMillis() - now})
    r
  }

  def totalFiles              = _totalFiles
  def totalFolders            = _totalFolders
  def totalFoldersCreated     = _totalFoldersCreated
  def totalUploads            = _totalUploads
  def totalExisting           = _totalExisting
  def totalExistingFolders    = _totalExistingFolders
  def totalErrors             = _totalErrors
  def uploadedBytes           = FileSize(_totalBytesUploaded, Bytes)
  def durationUploading: Duration = FiniteDuration(_durationUploading, MILLISECONDS)
  def durationCalculatingSha1: Duration = FiniteDuration(_durationCalculatingSha1, MILLISECONDS)
  def sha1Bytes               = FileSize(_totalBytesCalculatedSha1, Bytes)
  def sha1RatePerSecond       = FileRate.perSecond(durationCalculatingSha1, sha1Bytes)
  def uploadRatePerSecond     = FileRate.perSecond(durationUploading, uploadedBytes)
}
