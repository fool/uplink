package net.filemafia.fs

import java.io.{FileInputStream, File}
import java.security.MessageDigest
import javax.xml.bind.annotation.adapters.HexBinaryAdapter

import net.filemafia.uplink.Either.FutureEither
import net.filemafia.uplink.{Conversions, UplinkError}
import net.filemafia.uplink.Metrics

object Checksum {
  val Sha1BufferSize = new FileSize(8192, Bytes)

  def sha1(file: File): FutureEither[String] = {
    Conversions.blockToFutureEither(() => {
      val checksum = Metrics.timeSha1( () => {

        val sha1 = MessageDigest.getInstance("SHA-1")
        val input = new FileInputStream(file)

        val buffer = new Array[Byte](Sha1BufferSize.bytes.toInt)
        var length = input.read(buffer)

        while (length != -1) {
          sha1.update(buffer, 0, length)
          length = input.read(buffer)
        }
        new HexBinaryAdapter().marshal(sha1.digest())
      })
      Metrics.incrementSha1Bytes(file.length())
      checksum
    }, ErrorGeneratingSha1(file))
  }
}


case class ErrorGeneratingSha1(file: File) extends UplinkError {
  def message = s"failed to generate sha1 for $file"
}
