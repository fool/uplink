package net.filemafia.fs

import grizzled.slf4j.Logging
import scala.concurrent.duration._

case class FileRate(duration: Duration, timeUnit: TimeUnit, amount: FileSize) extends Logging {
  val sizeUnit: FileSystemUnit = amount.lowestUnit
  def per(targetAmount: FileSystemUnit, targetTime: Duration): BigDecimal = {
    FileRate.perTime(duration, amount, targetTime).as(targetAmount)
  }

  def timeUnitString = timeUnit.toString.toLowerCase.stripSuffix("s")
  def timeString: String = if (amount.as(sizeUnit) == BigDecimal(1)) timeUnitString else s"${amount.as(sizeUnit)}${timeUnit.toString.toLowerCase}"

  override def toString = lowestUnitString
  def lowestUnitString = s"${amount.lowestUnitString()}/$timeString"
}

object FileRate extends Logging {
  def perSecond(duration: Duration, amount: FileSize): FileSize = {
    perTime(duration, amount, new FiniteDuration(1, SECONDS))
  }

  def toUnit(duration: Duration, unit: TimeUnit): BigDecimal = {
    unit match {
      case NANOSECONDS  => BigDecimal(duration.toNanos)
      case MICROSECONDS => BigDecimal(duration.toNanos) / Math.pow(1000, 1)
      case MILLISECONDS => BigDecimal(duration.toNanos) / Math.pow(1000, 2)
      case SECONDS      => BigDecimal(duration.toNanos) / Math.pow(1000, 3)
      case MINUTES      => BigDecimal(duration.toNanos) / Math.pow(1000, 3) / 60
      case HOURS        => BigDecimal(duration.toNanos) / Math.pow(1000, 3) / 60 / 60
      case DAYS         => BigDecimal(duration.toNanos) / Math.pow(1000, 3) / 60 / 60 / 24
    }
  }

  def perTime(duration: Duration, amount: FileSize, destinationTime: Duration): FileSize = {
    val secondsTranslate = toUnit(duration, destinationTime.unit)
    if (secondsTranslate == BigDecimal(0)) {
      FileSize(0, Bits)
    }
    else {
      FileSize((destinationTime.toNanos * amount.bits) / duration.toNanos, Bits)
    }
  }
}