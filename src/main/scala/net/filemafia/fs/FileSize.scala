package net.filemafia.fs

import grizzled.slf4j.Logging


case class FileSize(num: BigDecimal, units: FileSystemUnit) extends Logging {

  import FileSystemUnit._

  lazy val bits: BigDecimal = convert(num, units, Bits)

  def kiloBits: BigDecimal = convertBits(bits, KiloBits)

  def megaBits: BigDecimal = convertBits(bits, MegaBits)

  def gigaBits: BigDecimal = convertBits(bits, GigaBits)

  def teraBits: BigDecimal = convertBits(bits, TeraBits)

  def bytes: BigDecimal = convertBits(bits, Bytes)

  def kiloBytes: BigDecimal = convertBits(bits, KiloBytes)

  def megaBytes: BigDecimal = convertBits(bits, MegaBytes)

  def gigaBytes: BigDecimal = convertBits(bits, GigaBytes)

  def teraBytes: BigDecimal = convertBits(bits, TeraBytes)

  def as(unit: FileSystemUnit): BigDecimal = {
    unit match {
      case Bits => bits
      case KiloBits => kiloBits
      case MegaBits => megaBits
      case GigaBits => gigaBits
      case TeraBits => teraBits
      case Bytes => bytes
      case KiloBytes => kiloBytes
      case MegaBytes => megaBytes
      case GigaBytes => gigaBytes
      case TeraBytes => teraBytes
    }
  }

  def >(other: FileSize): Boolean = bits > other.bits

  def >=(other: FileSize): Boolean = bits >= other.bits

  def <=(other: FileSize): Boolean = bits <= other.bits

  def <(other: FileSize): Boolean = bits < other.bits

  def +(other: FileSize): FileSize = FileSize(bits + other.bits, Bits)

  def -(other: FileSize): FileSize = FileSize(bits - other.bits, Bits)

  def *(other: FileSize): FileSize = FileSize(bits * other.bits, Bits)

  def /(other: FileSize): FileSize = FileSize(bits / other.bits, Bits)

  override def toString = s"$num $units"


  def lowestUnit: FileSystemUnit = {
    if (teraBytes > 1) {
      TeraBytes
    }
    else if (gigaBytes > 1) {
      GigaBytes
    }
    else if (megaBytes > 1) {
      MegaBytes
    }
    else if (kiloBytes > 1) {
      KiloBytes
    }
    else {
      Bytes
    }
  }

  def lowestUnitString(precision: Int = 2): String =
    s"%.0${precision}f ${lowestUnit.abbreviation}".format(as(lowestUnit))
}


sealed trait FileSystemUnit extends Logging {
  /** how many bits produce one of the units you are in? Bits = 1, Bytes = 8 */
  def grouping: Int
  /** how many times do you have to raise 1024? Kilo* = 1, Mega* = 2  */
  def power: Int

  def abbreviation: String
  def name: String
  override def toString = name
}

object FileSystemUnit {
  def toBits(amount: BigDecimal, units: FileSystemUnit): BigDecimal = {
    val base = (1 to units.power).foldLeft(amount)((accumulator, index) => accumulator * 1024)
    base * units.grouping
  }

  def convert(amount: BigDecimal, units: FileSystemUnit, targetUnits: FileSystemUnit):BigDecimal = {
    convertBits(toBits(amount, units), targetUnits)
  }

  def convertBits(bits: BigDecimal, targetUnits: FileSystemUnit):BigDecimal = {
    bits / Math.pow(1024, targetUnits.power) / targetUnits.grouping
  }
}

sealed trait Bit {
  lazy val grouping = 1
}

case object Bits extends FileSystemUnit with Bit {
  val abbreviation = "b"
  val power = 0
  val name = "bits"
}

case object KiloBits extends FileSystemUnit with Bit {
  val abbreviation = "kb"
  val power = 1
  val name = "kilobits"
}

case object MegaBits extends FileSystemUnit with Bit {
  val abbreviation = "mb"
  val power = 2
  val name = "megabits"
}

case object GigaBits extends FileSystemUnit with Bit {
  val abbreviation = "gb"
  val power = 3
  val name = "gigabits"
}

case object TeraBits extends FileSystemUnit with Bit {
  val abbreviation = "tb"
  val power = 4
  val name = "terabits"
}

/** Byte is taken */
sealed trait BitGroup {
  lazy val grouping = 8
}

case object Bytes extends FileSystemUnit with BitGroup {
  val abbreviation = "B"
  val power = 0
  val name = "bytes"
}


case object KiloBytes extends FileSystemUnit with BitGroup {
  val abbreviation = "KB"
  val power = 1
  val name = "kilobytes"
}


case object MegaBytes extends FileSystemUnit with BitGroup {
  val abbreviation = "MB"
  val power = 2
  val name = "megabytes"
}

case object GigaBytes extends FileSystemUnit with BitGroup {
  val abbreviation = "GB"
  val power = 3
  val name = "gigabytes"
}


case object TeraBytes extends FileSystemUnit with BitGroup {
  val abbreviation = "TB"
  val power = 4
  val name = "terabytes"
}

