package net.filemafia.date

import java.time.{ZoneOffset, ZonedDateTime}

import grizzled.slf4j.Logging

object DateCompare extends Logging {

  def isExpired(time: ZonedDateTime): Boolean = {
    time.isBefore(utcNow)
  }

  def utcNow = ZonedDateTime.now(ZoneOffset.UTC)
}
