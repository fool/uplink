package net.filemafia.http

import java.util.concurrent.TimeUnit

import net.filemafia.uplink.UplinkArguments
import org.apache.http.client.HttpClient
import org.apache.http.client.config.{RequestConfig, CookieSpecs}
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.{BasicCookieStore, HttpClients}
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager



object HttpClientMaker {
  /* this one client is shared for the entire program */
  def httpClient(uplinkArguments: UplinkArguments): HttpClient = {

    val multi150 = (uplinkArguments.multi * 1.5).toInt
    val connectionManager = new PoolingHttpClientConnectionManager()
    connectionManager.setMaxTotal(multi150)
    connectionManager.setDefaultMaxPerRoute(multi150)
    connectionManager.closeExpiredConnections()
    connectionManager.closeIdleConnections(1, TimeUnit.SECONDS)


    val requestConfigBuilder = RequestConfig.custom()
    //requestConfigBuilder.setCookieSpec(CookieSpecs.BEST_MATCH)
    requestConfigBuilder.setConnectTimeout(10000)
    requestConfigBuilder.setSocketTimeout(10000)
    requestConfigBuilder.setConnectionRequestTimeout(10000)
    val requestConfig = requestConfigBuilder.build()

    val cookieStore = new BasicCookieStore()

    val context = HttpClientContext.create()
    context.setCookieStore(cookieStore)


    val b = HttpClients.custom()
    b.setDefaultRequestConfig(requestConfig)
    b.setDefaultCookieStore(cookieStore)
    b.disableRedirectHandling()
    b.setConnectionManager(connectionManager)
    b.setRetryHandler(new RetryEverything(uplinkArguments.httpRetries))
    b.build()
  }
}
