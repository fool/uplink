package net.filemafia.http

import java.io.{File, FilterOutputStream, OutputStream}
import grizzled.slf4j.Logging
import org.apache.http.HttpEntity
import org.apache.http.entity.HttpEntityWrapper

/**
 * HttpEntityWrapper with a progress callback, based on:
 *
 * https://raw.githubusercontent.com/x2on/gradle-hockeyapp-plugin/master/src/main/groovy/de/felixschulze/gradle/util/ProgressHttpEntityWrapper.groovy
 * http://stackoverflow.com/a/7319110/268795
 */
class   ProgressHttpEntityWrapper(entity: HttpEntity, progressCallback: ProgressListener)
extends HttpEntityWrapper(entity) {
  override def writeTo(out: OutputStream): Unit = {
    this.wrappedEntity.writeTo(out match {
      case progress: ProgressFilterOutputStream => progress
      case other => new ProgressFilterOutputStream(other, progressCallback, getContentLength)
    })
  }
}

class   ProgressFilterOutputStream(out: OutputStream, progressCallback: ProgressListener, totalBytes: Long)
extends FilterOutputStream(out) {
  private var transferred: Long = 0
  private val totalDouble = totalBytes.toDouble

  override def write(bytes: Array[Byte], offset: Int, length: Int): Unit = {
    out.write(bytes, offset, length)
    transferred += length
    progressCallback.progress(currentProgress)
  }
  
  override def write(byte: Int): Unit = {
    out.write(byte)
    transferred += 1
    progressCallback.progress(currentProgress)
  }

  def currentProgress: Double = {
    (this.transferred / totalDouble) * 100.0
  }
}


trait ProgressListener {
  def progress(percent: Double): Unit
}

object NoProgress extends ProgressListener {
  def progress(percent: Double) = ()
}

case class ProgressStep(file: File, steps: Int = ProgressStep.DefaultProgressPercent)
extends    ProgressListener
with       Logging {
  var lastStep = 0.0
  def progress(percent: Double): Unit = {
    if (percent - lastStep > steps) {
      lastStep = percent
      logger.info(s"File $file uploading, ${percent.round}%")
    }
    /* in case of retries, need to reset progress */
    if (lastStep > percent) {
      lastStep = percent
    }
  }
}

object ProgressStep {
  val DefaultProgressPercent = 25
}
