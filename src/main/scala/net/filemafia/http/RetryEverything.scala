package net.filemafia.http

import java.io.IOException

import grizzled.slf4j.Logging
import org.apache.http.client.HttpRequestRetryHandler
import org.apache.http.protocol.HttpContext

class RetryEverything(max: Int) extends HttpRequestRetryHandler with Logging {
  def retryRequest(exception: IOException, executionCount: Int, context: HttpContext): Boolean = {
    if (executionCount < max)
      logger.info(s"Retry [$executionCount of $max] exception: ${exception.getMessage}")
    executionCount < max
  }
}
