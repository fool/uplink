package net.filemafia.http

import java.io.File
import java.net.URI
import java.util.ArrayList
import grizzled.slf4j.Logging
import net.filemafia.box.LoginResult
import net.filemafia.fs.{Bytes, FileSize}
import net.filemafia.uplink.{UplinkError, Conversions}
import net.filemafia.uplink.Either.ReaderEither
import org.apache.http.entity.StringEntity
import org.apache.http.entity.mime.content.{StringBody, FileBody}
import org.apache.http.{Header, HttpResponse}
import org.apache.http.client.HttpClient
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.{HttpGet, HttpPost, HttpRequestBase}
import org.apache.http.client.utils.URIBuilder
import org.apache.http.message.{BasicHeader, BasicNameValuePair}
import scala.concurrent.Future
import scala.util.{Try,Success=>ScalaSuccess,Failure=>ScalaFailure}
import scalaz._, Scalaz._
import org.apache.http.entity.mime.{MultipartEntityBuilder, HttpMultipartMode}
import scala.collection.JavaConversions.asJavaCollection
import net.filemafia.uplink.ImplicitConversions._
import scala.concurrent.ExecutionContext.Implicits.global
import org.apache.http.util.EntityUtils
import net.filemafia.uplink.Metrics

object Http extends Logging {

  def buildUrl(baseUrl: String, queryParams: Map[String, String]): URI = {
    queryParams.foldLeft(new URIBuilder(baseUrl)) {
      case (builder: URIBuilder, (name: String, value: String)) => builder.addParameter(name, value)
    }.build()
  }

  def upload[A](url: URI, headers: Seq[Header], data: Map[String, String], file: File, sizeLimit: FileSize, progressStep: Int)(responseHandler: => HttpResponse => A): ReaderEither[HttpClient, A] = {
    Reader((httpClient) => {
      Future {
        withPostRequest(url,  {
          case request: HttpPost =>
            val r = Metrics.timeUpload(() => {
              headers.foreach(request.addHeader)

              val fileBody = new FileBody(file)
              val multipartEntity = MultipartEntityBuilder.create()
              multipartEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
              multipartEntity.addPart("file", fileBody)
              data.foreach {
                             case (k, v) =>
                               multipartEntity.addTextBody(k, v)
                           }


              val fileSize = FileSize(file.length(), Bytes)
              val progressListener: ProgressListener =
                if (fileSize > sizeLimit)
                  ProgressStep(file, progressStep)
                else
                  NoProgress
              logger.debug(s"Needs progress? ${fileSize.megaBytes} > ${sizeLimit.megaBytes} = ${fileSize > sizeLimit}   ProgressListener: $progressListener")


              val progress = new ProgressHttpEntityWrapper(multipartEntity.build(), progressListener)
              request.setEntity(progress)
              logger.trace(s"Uploading ${request.getRequestLine}")

              val response = httpClient.execute(request)
              Try(responseHandler(response)) match {
                case ScalaSuccess(value) => Conversions.success(value)
                case ScalaFailure(t) => Conversions.failure(ResponseCallbackFailed(request, response, t))
              }
            })
            Metrics.incrementUploadedBytes(file.length())
            r
        })
      }.asFutureEither(HttpFailure(url))
    })
  }

  protected def withCloseable[A, C](thing: A, callback: A => C, close: () => Unit): C = {
    val result = Try(callback(thing))
    close()
    result match {
      case ScalaSuccess(callbackResult) => callbackResult
      case ScalaFailure(t)              => throw t
    }
  }

  protected def withResponse[A](response: HttpResponse, callback: HttpResponse => A): A = {
    withCloseable(response, callback, () => EntityUtils.consume(response.getEntity))
  }

  protected def withRequest[A, H <: HttpRequestBase](request: H, callback: H => A): A = {
    withCloseable(request, callback, () => request.releaseConnection())
  }

  protected def withGetRequest[A](uri: URI, callback: HttpGet => A): A = {
    withRequest(new HttpGet(uri), callback)
  }

  protected def withPostRequest[A](uri: URI, callback: HttpPost => A): A = {
    withRequest(new HttpPost(uri), callback)
  }


  def postForm[A](url: URI, headers: Seq[Header] = Seq.empty, data: Map[String, String])(responseHandler: => HttpResponse => A): ReaderEither[HttpClient, A] = {
    Reader((httpClient) => {
      Future {
        withPostRequest(url, {
          case request: HttpPost =>
            val params = data.map { case (name: String, value: String) => new BasicNameValuePair(name, value) }

            request.setHeaders(headers.toArray)
            request.setEntity(new UrlEncodedFormEntity(new ArrayList[BasicNameValuePair](asJavaCollection(params))))

            val result = withResponse(httpClient.execute(request), responseHandler)
            Conversions.success(result)
        })
      }.asFutureEither(HttpFailure(url))
    })
  }

  val DefaultJsonHeaders: Seq[Header] = Seq(
    new BasicHeader("Content-Type", "application/json"),
    new BasicHeader("Content-Encoding", "utf8")
  )
  def postJson[A](url: URI, headers: Seq[Header], data: String)(responseHandler: => HttpResponse => A): ReaderEither[HttpClient, A] = {
    Reader((httpClient) => {
      Future {
        withPostRequest(url, {
          case request: HttpPost =>
            request.setEntity(new StringEntity(data))
            request.setHeaders((DefaultJsonHeaders ++ headers).toArray)

            logger.info(s"POSTING $url WITH HEADERS ${(DefaultJsonHeaders ++ headers)}")
                        logger.info(s"Body is $data")
            val result = withResponse(httpClient.execute(request), responseHandler)
            Conversions.success(result)
        })
      }.asFutureEither(HttpFailure(url))
    })
  }

  def get[A](url: URI, headers: Seq[Header])(responseHandler: => HttpResponse => A): ReaderEither[HttpClient, A] = {
    Reader((httpClient) => {
      Future {
        withGetRequest(url, {
          case request: HttpGet =>
            request.setHeaders(headers.toArray)

            val result = withResponse(httpClient.execute(request), responseHandler)
            Conversions.success(result)
        })
      }.asFutureEither(HttpFailure(url))
    })
  }
}


case class BadHttpStatus(request: HttpRequestBase, response: HttpResponse) extends UplinkError {
  def message: String = s"Received bad http status: ${response.getStatusLine.getStatusCode} for $request"
}
case class LoginFailure(loginResult: LoginResult) extends UplinkError {
  def message: String = s"Received bad response on logging in for $loginResult"
}

case class BadHttpStatusException(request: HttpRequestBase, response: HttpResponse)
extends UplinkException(s"Received bad http status: ${response.getStatusLine.getStatusCode} for $request")

case class HttpRequestFailed(request: HttpRequestBase) extends UplinkError {
  val message = s"Error sending http request $request"
}

case class HttpFailure(uri: URI) extends UplinkError {
  val message = s"Error sending http to $uri"
}

case class ResponseCallbackFailed(request: HttpRequestBase, response: HttpResponse, throwable: Throwable) extends UplinkError {
  val message = s"Error calling http response callback $request $response $throwable"
}

abstract class UplinkException(message: String) extends Exception(message)