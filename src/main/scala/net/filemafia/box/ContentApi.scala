package net.filemafia.box

import java.io.File
import java.net.URI

import grizzled.slf4j.Logging
import net.filemafia.http.{UplinkException, Http}
import net.filemafia.uplink.Either.ReaderEither
import org.apache.http.message.BasicHeader
import org.apache.http.{Header, HttpResponse}
import org.apache.http.client.HttpClient
import net.filemafia.uplink.{UplinkArguments, UplinkError, FutureEither, Conversions}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.Source
import scala.util.parsing.json.JSON
import scalaz._, Scalaz._


object ContentApi
extends Logging {

  val BaseUrl = "https://api.box.com/2.0"
  val UploadUrl = new URI("https://upload.box.com/api/2.0/files/content")
  val Limit1000 = Limit(1000)

  def createBoxFolder(folderName: String, parentFolder: BoxFolder): ReaderEither[BoxApi, BoxFolder] = {
    Reader((boxApi) => {
      val url = new URI(s"$BaseUrl/folders")
      val data = BoxFolderCreate.toJson(folderName, parentFolder)
      for {
        headers <- headers()(boxApi)
        createFolderResponse <- Http.postJson(url, headers, data)(CreateFolderResponse.apply)(boxApi.httpClient)
        boxFolder <- getBoxFolder(createFolderResponse.folderId)(boxApi)
      } yield boxFolder
    })
  }

  def getBoxFolder(folderId: String): ReaderEither[BoxApi, BoxFolder] = {
    Reader((boxApi) => {
      for {
        folderResponses <- getBoxFolderRecursive(folderId, Limit1000, 0)(boxApi)
      } yield BoxFolder(folderId, folderResponses)
    })
  }

  protected def getBoxFolderRecursive(folderId: String, limit: Limit, offset: Long): ReaderEither[BoxApi, Seq[Folder]] = {
    Reader((boxApi) => {
      for {
        folderInfoResponse <- folderInfo(folderId, limit, offset)(boxApi)
        moreItems <- {
          if (folderInfoResponse.size != 0) {
            getBoxFolderRecursive(folderId, limit, folderInfoResponse.nextOffset)(boxApi)
          } else {
            Conversions.futureEitherSuccess(Seq.empty[Folder])
          }
        }
      } yield Seq(Seq(folderInfoResponse), moreItems).flatten
    })
  }

  def fileInfo(boxFileId: String): ReaderEither[BoxApi, BoxFile] = {
    Reader((clients) => {
      val url = new URI(s"$BaseUrl/files/$boxFileId")
      for {
        headers <- headers()(clients)
        fileResponse <- Http.get(url, headers)(BoxFile.apply)(clients.httpClient)
        goodFile <- FutureEither.constraint[BoxFile](fileResponse, _.httpStatus == 200, FileNotFound(boxFileId))
      } yield goodFile
    })
  }

  def folderInfo(boxFolderId: String, limit: Limit = Limit(100), offset: Long): ReaderEither[BoxApi, Folder] = {
    Reader((clients) => {
      val url = new URI(s"$BaseUrl/folders/$boxFolderId/items?limit=$limit&offset=$offset")
      for {
        headers <- headers()(clients)
        folderResponse <- Http.get(url, headers)(Folder.apply)(clients.httpClient)
        goodFolder <- FutureEither.constraint[Folder](folderResponse, _.httpStatus == 200, FolderNotFound(boxFolderId, folderResponse.content))
      } yield goodFolder
    })
  }

  def upload(file: File, boxFolder: BoxFolder): ReaderEither[BoxApi, UploadResponse] = {
    Reader((clients) => {
      val data = Map("attributes" -> s"""{"name":"${file.getName}","parent":{"id":"${boxFolder.id}"}}""")

      for {
        headers <- headers()(clients)
        uploadResponse <- Http.upload(UploadUrl, headers, data, file, clients.arguments.sizeLimit,
                     clients.arguments.progressPercent)(UploadResponse.apply)(clients.httpClient)
      } yield uploadResponse
    })
  }

  def headers(): ReaderEither[BoxApi, Seq[Header]] = {
    Reader((clients) => {
      val s = Seq(new BasicHeader("Authorization", s"Bearer ${clients.accessToken.accessToken(clients.httpClient)}"))
      Conversions.futureEitherSuccess(s)
    })
  }
}

case class BoxApi(httpClient: HttpClient, accessToken: AccessToken, arguments: UplinkArguments)

case class FolderNotFound(folderId: String, content: String) extends UplinkError {
  def message = s"Folder id not found: $folderId  $content"
}

case class FileNotFound(fileId: String) extends UplinkError {
  def message = s"File id not found: $fileId"
}

case class UploadResponse(response: HttpResponse) {
  val httpStatus: Int = response.getStatusLine.getStatusCode

  val content: String = Source.fromInputStream(response.getEntity.getContent).mkString

  def sha1: String = {
    //{"type":"error","status":409,"code":"item_name_in_use","context_info":{"conflicts":{"type":"file","id":"34859135731","file_version":{"type":"file_version","id":"34217734229","sha1":"41c5985fc771b6ecfe8feaa99f8fa9b77ac7d6ce"},"sequence_id":"0","etag":"0","sha1":"41c5985fc771b6ecfe8feaa99f8fa9b77ac7d6ce","name":"test3.txt"}},"help_url":"http:\/\/developers.box.com\/docs\/#errors","message":"Item with the same name already exists","request_id":"170831822455cecc36f1923"}
    val jsonO: Option[String] = JSON.parseFull(content) match {
      case Some(a) =>
        val msa = a.asInstanceOf[Map[String, Any]]
        val contextInfo = msa("context_info").asInstanceOf[Map[String, Any]]
        val conflicts = contextInfo("conflicts").asInstanceOf[Map[String, Any]]
        Some(conflicts("sha1").asInstanceOf[String])
      case _ => None
    }
    jsonO.getOrElse(throw Sha1ParseException(content))
  }
}

abstract class BoxApiParseException(message: String) extends UplinkException(message)

case class Sha1ParseException(content: String)
extends BoxApiParseException(s"Unable to parse sha1 from box response $content")


case class CreateFolderResponse(response: HttpResponse)
extends Logging {
  val httpStatus: Int = response.getStatusLine.getStatusCode

  val content: String = Source.fromInputStream(response.getEntity.getContent).mkString

  logger.debug(s"Create folder response: $httpStatus $content")

  val alreadyExists = true

  val folderId = httpStatus match {
    case 201 => JSON.parseFull(content).get.asInstanceOf[Map[String, Any]]("id").asInstanceOf[String]
    case 409 => JSON.parseFull(content).get.asInstanceOf[Map[String, Any]]("context_info").asInstanceOf[Map[String, Any]]("conflicts").asInstanceOf[Seq[Map[String, Any]]].head("id").asInstanceOf[String]
    case _ => throw FolderParseError(content)
  }
}

case class FolderParseError(content: String)
extends    BoxApiParseException(s"unable to parse: $content")

case class InvalidLimitException(limit: Long) extends UplinkException(s"Invalid limit $limit")

case class Limit(limit: Long) {
  if (limit > 1000 || limit < 0) {
    throw InvalidLimitException(limit)
  }
  override def toString = limit.toString
}

case class Folder(httpResponse: HttpResponse) {
  val httpStatus: Int = httpResponse.getStatusLine.getStatusCode

  val content: String = Source.fromInputStream(httpResponse.getEntity.getContent).mkString

  private lazy val json: Map[String, Any] = JSON.parseFull(content).get.asInstanceOf[Map[String, Any]]

  def items: Seq[MiniItem] = {
    json("entries").asInstanceOf[Seq[Map[String, String]]].map {
      item => MiniItem(
        ItemType(item("type")),
        item("id"),
        item("sequence_id"),
        item("etag"),
        item("name")
      )
    }
  }

  def id: String = json("id").asInstanceOf[String]
  def size: Long = items.size
  def limit: Limit = Limit(json("limit").asInstanceOf[Double].round)
  def offset: Long = json("offset").asInstanceOf[Double].round
  def nextOffset: Long = offset + limit.limit
}

case class BoxFolder (id: String, size: Long, items: Seq[MiniItem]) {
  def hasFile(name: String): Boolean = items.exists(_.name == name)
  def subItem(name: String): Option[MiniItem] = items.find(_.name == name)
}

object BoxFolder {
  def apply(boxFolderId: String, folders: Seq[Folder]): BoxFolder = {
    val items = folders.flatMap(_.items)
    val size = folders.map(_.size).sum
    BoxFolder(boxFolderId, size, items)
  }
}

case class BoxFile(httpResponse: HttpResponse) {
  val httpStatus: Int = httpResponse.getStatusLine.getStatusCode

  val content: String = Source.fromInputStream(httpResponse.getEntity.getContent).mkString

  private lazy val json: Map[String, Any] = JSON.parseFull(content).get.asInstanceOf[Map[String, Any]]

  def sha1: String = json("sha1").asInstanceOf[String]
}


case class MiniItem(itemType: ItemType, id: String, sequenceId: String, etag: String, name: String)

object ItemType {
  val All: Seq[ItemType] = Seq(FileType, FolderType)
  def apply(name: String): ItemType = {
    All.find(_.name == name).getOrElse(throw InvalidItemTypeException(name))
  }
}
sealed abstract class ItemType(val name: String)
case object FileType extends ItemType("file")
case object FolderType extends ItemType("folder")

case class InvalidItemTypeException(name: String) extends UplinkException(s"Invalid item type: $name")

case class LocalFolderIsRemoteFileError(name: String, parent: BoxFolder, item: MiniItem) extends UplinkError {
  def message = s"Expected $name to not exist or be a folder inside box folder: ${parent.id}, there's a file with this name: $item"
}
case class LocalFileIsRemoteFolderError(parent: BoxFolder, remoteFolder: MiniItem, localFile: File) extends UplinkError {
  def message = s"Expected ${localFile.getName} to not exist on box or be a file inside box folder: ${parent.id}, actually there's a folder with this name: $remoteFolder"
}