package net.filemafia.box

import grizzled.slf4j.Logging
import net.filemafia.http.UplinkException
import net.filemafia.uplink.{Uplink, UplinkError, Conversions}
import net.filemafia.uplink.Either.{FutureEither, UplinkEither, ReaderEither}
import org.apache.http.client.HttpClient
import net.filemafia.uplink.ImplicitConversions._
import scala.concurrent.{Await, Future}
import scalaz._, Scalaz._
import scala.concurrent.ExecutionContext.Implicits.global


case class AccessToken(loginParams: LoginParams)
extends Logging {
  var tokens: Option[Tokens] = None

  var tokenHit = 0
  var tokenFetch = 0

  /** blocking method, not a future */
  def accessToken(httpClient: HttpClient): String = {
    synchronized {
      val start = System.currentTimeMillis
      logger.debug("Entering critical section")
      val currentAccessToken = tokens match {
        case Some(cachedTokens) if !cachedTokens.isAccessTokenExpired =>
          tokenHit = tokenHit + 1
          logger.debug("Inside critical section and access token cache is fresh, using cached access code")
          cachedTokens.accessToken
        case _ =>
          tokenFetch = tokenFetch + 1
          logger.debug(s"Inside critical section, cache miss on access token going to get a new one")
          val futureTokens: Future[Tokens] = AccessToken.accessTokenSynchronized(tokens, loginParams)(httpClient).run.map {
            case \/-(tok) => tok
            case -\/(error) => throw LoginException(error)
          }
          /* you have to run the future now since you are in synchronized lock */
          val newTokens = Await.result(futureTokens, Uplink.AccessTokenTimeout)
          tokens = Some(newTokens)
          newTokens.accessToken
      }
      logger.debug(s"accessToken() took\t${System.currentTimeMillis() - start}ms")
      logger.debug("Exiting critical section")
      currentAccessToken
    }
  }

  def hitRate: Double = tokenHit / (tokenHit + tokenFetch)
}

object AccessToken extends Logging {
  def accessTokenSynchronized(tokens: Option[Tokens], loginParams: LoginParams): ReaderEither[HttpClient, Tokens] = {
    Reader((httpClient) => {
      tokens match {
        case Some(token) if !token.isAccessTokenExpired =>
          logger.debug("Using existing access token")
          Conversions.futureEitherSuccess(token)
        case Some(token) if !token.isRefreshTokenExpired =>
          logger.debug("Access token expired but have refresh token")
          val r: Future[FutureEither[Tokens]] = AutoLogin.refresh(token, loginParams)(httpClient).run.map {
            case \/-(tok) =>
              logger.debug("Refresh token success, got new access tokens")
              Conversions.futureEitherSuccess(tok)
            case -\/(error) =>
              logger.debug("Refresh token failure, trying to do the whole flow again with login")
              AutoLogin.accessToken(loginParams)(httpClient)
          }
          Conversions.futureToFutureEither(r.flatMap(_.run), RefreshTokensFailure)

        case _ =>
          logger.debug("Have never logged in before, logging int to get new tokens")
          for {
            a <- AutoLogin.accessToken(loginParams)(httpClient)
          } yield a
      }
    })
  }
}


case object RefreshTokensFailure extends UplinkError {
  def message = s"Unable to refresh tokens"
}

case class LoginException(error: UplinkError)
extends    UplinkException(s"Unable to log in to box: $error")
