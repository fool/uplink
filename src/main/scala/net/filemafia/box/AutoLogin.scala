package net.filemafia.box

import java.net.URI
import java.time.ZonedDateTime

import grizzled.slf4j.Logging
import net.filemafia.date.DateCompare
import net.filemafia.http.{LoginFailure, Http}
import net.filemafia.uplink.Either.{FutureEither, ReaderEither}
import net.filemafia.uplink.ImplicitConversions._
import net.filemafia.uplink.{Conversions, UplinkError}
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.utils.URLEncodedUtils
import scala.collection.JavaConverters._
import scala.io.Source
import scala.util.Try
import scala.util.parsing.json.JSON
import scalaz._, Scalaz._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{FiniteDuration, DAYS}
import scala.concurrent.Future

object AutoLogin extends Logging {


  def login(params: LoginParams): ReaderEither[HttpClient, LoginResult] = {
    Reader((httpClient) => {
      val url = Http.buildUrl("https://app.box.com/api/oauth2/authorize", Map(
        "response_type" -> "code",
        "client_id"     -> params.clientId,
        "state"         -> params.state
        //"redirect_uri"  -> params.redirectUri
      ))

      val data = Map[String, String](
        "login"                      -> params.username,
        "password"                   -> params.password,
        "login_submit"               -> "Authorizing...",
        "dologin"                    -> "1",
        "client_id"                  -> params.clientId,
        "response_type"              -> "code",
        //"redirect_uri"               -> params.redirectUri,
        "scope"                      -> "root_readwrite",
        "folder_id"                  -> "",
        "file_id"                    -> "",
        "state"                      -> params.state,
        "reg_step"                   -> "",
        "submit1"                    -> "1",
        "folder"                     -> "",
        "login_or_register_mode"     -> "login",
        "new_login_or_register_mode" -> "",
        "__login"                    -> "1",
        // Skip a GET on the page and dont have the RT the first time.
        // this works anyway
        //"request_token",            -> "44bccf0747cd3e97a1018600bbd599383895500f69c0d541f6732b2ac081debf",
        "_pw_sql"                    -> ""
      )
      logger.debug(s"Logging in to $url")
      try {
        for {
          loginResult <- Http.postForm(url, data = data)(LoginResult.apply)(httpClient)
          _ = logger.info(s"Validating login result....")
          _ <- LoginResult.validate(loginResult)
        } yield loginResult
      } catch {
        case t: Throwable =>
          logger.error(s"GOt exception here", t)
          throw t
      }
    })
  }

  def grant(params: LoginParams, loginResult: LoginResult): ReaderEither[HttpClient, GrantResult] = {
    Reader((httpClient) => {
      val ic = extractIc(loginResult.content)
      val requestToken = extractRequestToken(loginResult.content)
      val url = Http.buildUrl("https://app.box.com/api/oauth2/authorize", Map(
        "response_type" -> "code",
        "client_id"     -> params.clientId
      ))
      val postData = Map(
        "client_id"      -> params.clientId,
        "response_type"  -> "code",
        "redirect_uri"   -> params.redirectUri,
        "scope"          -> "root_readwrite",
        "file_id"        -> "",
        "state"          -> params.state,
        "doconsent"      -> "doconsent",
        "ic"             -> ic,
        "consent_accept" -> "Grant+access+to+Box",
        "request_token"  -> requestToken
      )
      logger.debug("Login successful, doing grant")
      logger.debug(s"Posting to grant with")
      val max = postData.keys.map(_.length).toSeq.sorted.reverse.head + 1
      logger.debug(s"max is $max")
      postData.foreach { case (k, v) => logger.debug(prepad(k, max) + v)}
      logger.debug(s"POST $url")
      for {
        grant <- Http.postForm(url, data=postData)(GrantResult.apply)(httpClient)
        _ <- GrantResult.validate(grant)
      } yield grant
    })
  }

  def prepad(string: String, min: Int): String = {
    if (string.length < min) {
      string + (" " * (min - string.length + 2))
    } else {
      string
    }
  }


  def accessToken(params: LoginParams): ReaderEither[HttpClient, Tokens] = {
    Reader((httpClient) => {
      for {
        loginResult  <- login(params)(httpClient)
        grantResult  <- grant(params, loginResult)(httpClient)
        accessCode   <- grantResult.accessCode
        tokens       <- tradeAccessCodeForAccessToken(accessCode, params)(httpClient)
      } yield tokens
    })
  }


  /*
  curl https://app.box.com/api/oauth2/token \ -d
  'grant_type=refresh_token&refresh_token={valid refresh token}&client_id={your_client_id}&client_secret={your_client_secret}' \ -X POST
   */
  def refresh(tokens: Tokens, loginParams: LoginParams): ReaderEither[HttpClient, Tokens] = {
    Reader((httpClient) => {
      val url = new URI("https://app.box.com/api/oauth2/token")
      val data = Map(
        "grant_type"    -> "refresh_token",
        "refresh_token" -> tokens.refreshToken,
        "client_id"     -> loginParams.clientId,
        "client_secret" -> loginParams.clientSecret
      )
      for {
        accessCodeResult <- Http.postForm(url, data=data)(AccessCodeResult)(httpClient)
        tokens <- accessCodeResult.parse
      } yield tokens
    })
  }

  def tradeAccessCodeForAccessToken(accessCode: String, params: LoginParams): ReaderEither[HttpClient, Tokens] = {
    Reader((httpClient) => {
      logger.debug("Grant succeeded, trading code for access token...")
      val url = new URI("https://api.box.com/oauth2/token")
      val postData = Map(
        "grant_type"    -> "authorization_code",
        "code"          -> accessCode,
        "client_id"     -> params.clientId,
        "client_secret" -> params.clientSecret
        //"redirect_uri"  -> params.redirectUri
      )
      logger.trace(s"Posting to $url to get access code with authorization code $accessCode and redirect uri ${params.redirectUri}")
       for {
         response <- Http.postForm(url, data=postData)(AccessCodeResult)(httpClient)
         tokens <- response.parse
       } yield tokens
    })
  }

  protected def extractIc(content: String): String = {
    val regex = """name="ic" value="([A-Za-z0-9]{64})""".r
    regex.findFirstMatchIn(content) match {
      case Some(value) => value.group(1)
      case None => ""
    }
  }

  protected def extractRequestToken(content: String): String = {
    val regex = """request_token = '([A-Za-z0-9]+)'""".r
    regex.findFirstMatchIn(content) match {
      case Some(value) => value.group(1)
      case None => ""
    }
  }
}

case class GrantResult(response: HttpResponse)
extends    Logging {
  val httpStatus: Int = response.getStatusLine.getStatusCode

  val accessCode: FutureEither[String] = {
    val headers = response.getAllHeaders
    val locationHeader = headers.find(_.getName.equalsIgnoreCase("location"))

    logger.debug(s"Got location header? $locationHeader")
    val location = locationHeader.flatMap {
      header =>
        val uri = new URI(header.getValue)
        val list = URLEncodedUtils.parse(uri, "UTF-8").asScala
        logger.debug(s"Looking for code in $list")
        list.find(_.getName == "code").map(_.getValue)
    }
    val error = BadAccessCode(response.toString)
    Conversions.toFutureEither(location.getOrUplinkError(error), error)
  }
  val content: String = Source.fromInputStream(response.getEntity.getContent).mkString
}

object GrantResult extends Logging {
  def validate(grantResult: GrantResult): FutureEither[GrantResult] = {
    val valid = grantResult.httpStatus == 302
    val either =
      if (valid)
        Conversions.success(grantResult)
      else {
        logger.debug(s"Grant got ${grantResult.httpStatus} but wanted 302 content: ${grantResult.content}")
        Conversions.failure(GrantFailed(grantResult))
      }

    Conversions.toFutureEither(either, GrantFailed(grantResult))
  }
}

case class LoginResult(httpResponse: HttpResponse) extends Logging {
  val content = Source.fromInputStream(httpResponse.getEntity.getContent).mkString
  val success = httpResponse.getStatusLine.getStatusCode == 200
  logger.debug(s"HTTP ${httpResponse.getStatusLine.getStatusCode} Content is $content")
}

object LoginResult extends Logging {

  def validate(loginResult: LoginResult): FutureEither[LoginResult] = {
    val valid = loginResult.success && loginResult.content.indexOf("There seems to be a problem with this app") == -1

    val either = if (valid) {
      Conversions.success(loginResult)
    } else {
      logger.info(s"Login result success? ${loginResult.success} has problem with app? ${loginResult.content.indexOf("There seems to be a problem with this app") == -1}")
      Conversions.failure(LoginFailure(loginResult))
    }
    Conversions.toFutureEither(either, LoginFailure(loginResult))
  }
}


case class AccessCodeResult(httpResponse: HttpResponse)
extends Logging {

  def parseBoxResponse(json: String): FutureEither[Map[String, String]] = {
    val jsonO: Option[Map[String, String]] = JSON.parseFull(json) match {
      case Some(a) => Some(a.asInstanceOf[Map[String, Any]].mapValues(_.toString))
      case _ => None
    }
    Conversions.toFutureEither(jsonO.getOrUplinkError(BadJson(json)), BadJson(json))
  }

  val content = Source.fromInputStream(httpResponse.getEntity.getContent).mkString

  def parse: FutureEither[Tokens] = {

    for {
      json1 <- parseBoxResponse(content)
      json = {
        logger.debug(s"json is $json1")
        json1
      }
      now = DateCompare.utcNow
      accessTokenExpiration <- Future(Try {
        json("expires_in").toDouble.round
      }.toOption.getOrUplinkError(CantParseExpiresIn(json("expires_in")))).asFutureEither(CantParseExpiresIn(json("expires_in")))
    }  yield Tokens(
      json("access_token").toString,
      json("refresh_token").toString,
      now.plusSeconds(accessTokenExpiration),
      now.plusSeconds(Box.RefreshTokenLifetime.toSeconds)
    )
  }
}

case class Tokens(accessToken: String,
                  refreshToken: String,
                  accessTokenExpiration: ZonedDateTime,
                  refreshTokenExpiration: ZonedDateTime)
extends Logging {
  def isAccessTokenExpired: Boolean = {
    DateCompare.isExpired(accessTokenExpiration)
  }
  def isRefreshTokenExpired: Boolean = DateCompare.isExpired(refreshTokenExpiration)
}


case class BadJson(response: String) extends UplinkError {
  def message = s"Invalid json: $response"
}
case class BadAccessCode(accessCode: String) extends UplinkError {
  def message = s"Invalid accessCode: $accessCode"
}

case class LoginFailed(result: LoginResult) extends UplinkError {
  def message = s"Login failed: $result"
}
case class GrantFailed(result: GrantResult) extends UplinkError {
  def message = s"Grant failed: $result ${result.content}"
}

case class CantParseExpiresIn(expiresIn: String) extends UplinkError {
  def message = s"failed to parse expiresIn as double $expiresIn"
}


case class LoginParams(clientId: String, clientSecret: String, username: String, password: String, state: String, redirectUri: String)

object Box {
  val RefreshTokenLifetime = new FiniteDuration(60, DAYS)
}