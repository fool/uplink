package net.filemafia.box

import org.json4s.JsonAST.{JObject, JField, JString}
import org.json4s.DefaultFormats
import org.json4s.native.Serialization

/**
 * Json Writer
 */
object BoxFolderCreate {
  implicit val formats = DefaultFormats
  def toJson(name: String, parent: BoxFolder): String = {
    Serialization.write(JObject(List(
      JField("name", JString(name)),
      JField("parent", JObject(List(JField("id", JString(parent.id)))))
    )))
  }
}
